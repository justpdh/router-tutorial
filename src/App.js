//import logo from './logo.svg';
import './App.css';
import React, { useContext, useEffect } from 'react';
import { Route, Routes, useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next'
import Lottie from 'react-lottie';

import * as Constant from "./context/Constant";
import * as LoginUtil from "./util/LoginUtil";

import animationData from './asset/image/loading.json'
import { GlobalContext } from "./context/index";

import Layout from './page/Layout';
import About from './page/About';
import Home from './page/Home';
import Profile from './page/Profile';
import Articles from './page/Articles';
import Article from './page/Article';
import NotFound from './page/NotFound';
import Login from './page/Login';
import MyPage from './page/MyPage';
import TestPage from './page/TestPage';

const App = () => {
    console.log('01----App');
    
    const navigate = useNavigate();
    const { i18n } = useTranslation();
    const { loginUser, setLoginUser, loading, setLoading, language } = useContext(GlobalContext);

    const lottieOption = {
        loop: true,
        autoplay: true,
        animationData,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };

    useEffect(() => {
        console.log('Layout----useEffect-syncLogin');
        console.log('loginUser=', loginUser);

        const userStorage = LoginUtil.getUserStorage();

        if (userStorage) {
            setLoading({type:Constant.LOADING_ACTION.PLUS});

            LoginUtil.isAvailUserStorage()
            .then((res) => {
                if (res.resultcode === 1) {
                    setLoading({type:Constant.LOADING_ACTION.PLUS});
                    LoginUtil.loginWithToken(userStorage['userId'], userStorage['authToken'])
                    .then((res) => {
                        if (res.resultcode === 1) {

                            const d = new Date();
                            const authPeriod = d.getTime() + (1000 * 60 * 10);

                            const us = {
                                'userId': userStorage['userId'],
                                'authToken': 'g5XrsTIytG6M+9juHCuzTj4tnKt1B40vMgJR2vaNiho=',
                                'authPeriod': authPeriod
                            };
                            LoginUtil.setUserStorage(JSON.stringify(us));

                            setLoginUser({
                                userId: userStorage['userId'],
                                userName: userStorage['userId'] + 'Name',
                                email: userStorage['userId'] + '@test.com'
                            });
                        }
                    })
                    .finally(() => {
                        setLoading({type:Constant.LOADING_ACTION.MINUS});
                    });
                } else {
                    LoginUtil.removeUserStorage();
                    setLoginUser(null);
                    navigate("/");
                }
            })
            .finally(() => {
                setLoading({type:Constant.LOADING_ACTION.MINUS});
            });
        } else {
            LoginUtil.removeUserStorage();
        }
    }, [navigate]);


    React.useEffect(() => {
        i18n.changeLanguage(language);
    }, [language]);


    return (
        <div>
            <div className={`loading-wrap ${loading.count > 0 ? "open" : ""}`}>
                <Lottie className="lottie" width={100} height={100} options={lottieOption} />
            </div>
            <Routes>
                <Route path="/" element={<Layout />} >
                    <Route index element={<Home />} />
                    <Route path="/about" element={<About />} />
                    <Route path="/profiles/:username" element={<Profile />} />
                    <Route path="/articles" element={<Articles />} >
                        <Route path=":id" element={<Article />} />
                    </Route>
                    <Route path="/login" element={<Login />} />
                    <Route path="/mypage" element={<MyPage />} />
                    <Route path="/testpage" element={<TestPage />} />
                </Route>
                <Route path="/*" element={<NotFound />} />
            </Routes>
        </div>
    );
};

export default App;
