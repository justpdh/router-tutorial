const translation = {
    'HOME': 'Home',
    'BACK':'Back',
    'ARTICLES':'Articles',
    'LOGIN':'Login',
    'LOGOUT':'Logout',
    'MY_PAGE':'My Page'
}

export default translation ;