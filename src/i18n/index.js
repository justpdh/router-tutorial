import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import en from "./en"
import de from "./de"
import ko from "./ko"
import it from "./it"


const resources = {
    en: {
        translation: en
    },
    de: {
        translation: de
    },
    ko: {
        translation: ko
    },
    it: {
        translation: it
    }
};

i18n
    .use(initReactI18next)
    .init({
        resources,
        fallbackLng: 'en',
    });

export default i18n;
