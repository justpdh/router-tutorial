const translation = {
    'HOME': '홈',
    'BACK':'뒤로가기',
    'ARTICLES':'게시물목록',
    'LOGIN':'로그인',
    'LOGOUT':'로그아웃',
    'MY_PAGE':'마이 페이지'
}

export default translation ;