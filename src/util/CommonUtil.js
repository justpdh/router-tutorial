const getNavigatorLanguage = () => {
    return navigator.language || navigator.userLanguage;
}

export {
    getNavigatorLanguage
}