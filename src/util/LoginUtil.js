const login = (id, password) => {
    console.log('LoginUtil.login', 'id=' + id + ', password=' + password);
    return new Promise((res, rej) => {
        return setTimeout(() => {
            return res({ resultcode: 1, user:{} });
        }, 100);
    });
}

const loginWithToken = (id, token) => {
    console.log('LoginUtil.loginWithToken', 'id=' + id + ', token=' + token);
    return new Promise((res, rej) => {
        return setTimeout(() => {
            return res({ resultcode: 1, user:{} });
        }, 100);
    });
}

const logout = () => {
    console.log('LoginUtil.logout');
    return new Promise((res, rej) => {
        return setTimeout(() => {
            //sessionStorage.clear()
            sessionStorage.removeItem('user');
            return res({ resultcode: 1 });
        }, 100);
    });
}

const setUserStorage = (userStorage) => {
    sessionStorage.setItem('user', userStorage);
}
const getUserStorage = () => {
    return sessionStorage.getItem('user') ? JSON.parse(sessionStorage.getItem('user')) : null;
}
const removeUserStorage = (userStorage) => {
    sessionStorage.removeItem('user');
}

const isAvailUserStorage = () => {
    console.log('LoginUtil.isAvailUserStorage');

    return new Promise((res, rej) => {
        const userStorage = sessionStorage.getItem('user');
        if(userStorage){
            return setTimeout(() => {
                const user = JSON.parse(userStorage);
                const curTime = new Date().getTime();
                if(parseFloat(user['authPeriod']) > curTime){
                    return res({ resultcode: 1 });
                }else{
                    return res({ resultcode: 0 });
                }
    
            }, 100);
        }else{
            return res({ resultcode: 0 });
        }
    });
}


export {
    login,
    logout,
    loginWithToken,
    setUserStorage,
    getUserStorage,
    removeUserStorage,
    isAvailUserStorage
}