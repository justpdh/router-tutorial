const TEST_ACTION = {
    ADD_FRUIT : 'ADD_FRUIT'
};

const LANGUAGE_ACTION = {
    SET : 'SET'
};

const LOADING_ACTION = {
    PLUS : 'PLUS',
    MINUS : 'MINUS'
};


export {
    TEST_ACTION,
    LANGUAGE_ACTION,
    LOADING_ACTION
}
