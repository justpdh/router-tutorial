import React from 'react';
import { createContext, useReducer, useState } from "react";
import * as Constant from "./Constant";
import * as CommonUtil from "../util/CommonUtil";

const GlobalContext = createContext({});


// const initLoginUser = {
//     userId:'NONE',
//     userName: 'NONE',
//     email: 'NONE'
// };

const initTestList = {
    computer: [
        { name: "pc", price: 100 },
        { name: "note-book", price: 200 },
    ],
    fruits: [
        { name: "banana", price: 10 },
        { name: "apple", price: 20 },
    ]
};

const testListReducer = (state, action) => {
    switch (action.type) {
        case Constant.TEST_ACTION.ADD_FRUIT:
            return {
                ...state,
                fruits: [...state.fruits, action.payload],
            };
        default:
            return state;
    }
};

const languageReducer = (state, action) => {
    switch (action.type) {
        case Constant.LANGUAGE_ACTION.SET:
            sessionStorage.setItem('userLang', action.value.lang);
            return action.value.lang;
        default:
            return state;
    }
};

const initLoading = {
    count: 0
};
const loadingReducer = (state, action) => {
    switch (action.type) {
        case Constant.LOADING_ACTION.PLUS:
            return {
                ...state,
                count: state.count + 1
            };
        case Constant.LOADING_ACTION.MINUS:
            return {
                ...state,
                count: state.count > 0 ? state.count - 1 : 0
            };
        default:
            return state;
    }
};



const availLanguageList = ['en', 'ko', 'de', 'it'];

const GlobalProvider = ({ children }) => {
    console.log('01----GlobalProvider');

    const [testList, setTestList] = useReducer(testListReducer, initTestList);
    //const pvTestList = { testList, setTestList };
    const [loginUser, setLoginUser] = useState(null);
    const [loading, setLoading] = useReducer(loadingReducer, initLoading);

    const userStorageLang = sessionStorage.getItem('userLang');
    const userLang = userStorageLang || availLanguageList.find((item) => CommonUtil.getNavigatorLanguage().startsWith(item) || availLanguageList[0]);
    const [language, setLanguage] = useReducer(languageReducer, userLang);

    const providValue = {
        testList, setTestList,
        loginUser, setLoginUser,
        language, setLanguage,
        loading, setLoading
    };

    return (
        <GlobalContext.Provider value={providValue}>
            {children}
        </GlobalContext.Provider>
    )
};

export { GlobalContext, GlobalProvider };