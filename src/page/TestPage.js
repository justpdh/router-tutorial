import { useContext } from "react";
import * as Constant from "../context/Constant";
import { GlobalContext } from "../context/index";

const TestPage = () => {
    console.log('01----TestPage');

    const {testList, setTestList } = useContext(GlobalContext);

    console.log(JSON.stringify(testList));

    return (
        <div>
            <h3>Homeplus</h3>
            <div>
                {testList && testList.computer && testList.computer.length > 0 &&
                    <ul>
                        {testList.computer.map((item, index) => {
                            return (<li key={index}>{item.name}</li>)
                        })}
                    </ul>
                }
            </div>
            <div>
                {testList && testList.fruits && testList.fruits.length > 0 &&
                    <ul>
                        {testList.fruits.map((item, index) => {
                            return (<li key={index}>{item.name}</li>)
                        })}
                    </ul>
                }
            </div>
            <div>
            <button
                onClick={() => {
                    setTestList({
                        type: Constant.TEST_ACTION.ADD_FRUIT,
                        payload: { name: "tomato", price: 300 },
                    });
                }}
            >
                Add fruit
            </button>
            </div>
        </div>
    );
}


export default TestPage;