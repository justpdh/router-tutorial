import React, { useContext, useState } from 'react';
import { useNavigate  } from "react-router-dom";

import * as Constant from "../context/Constant";
import { GlobalContext } from "../context/index";
import * as LoginUtil from "../util/LoginUtil";


const Login = () => {
    console.log('01----Login');
    const { setLoginUser, setLoading } = useContext(GlobalContext);

    const [userId, setUserId] = useState('');
    const [userPw, setUserPw] = useState('');

    const navigate = useNavigate();

    const onChangeUserId = e => {
        setUserId(e.target.value);
    };

    const onChangeUserPw = e => {
        setUserPw(e.target.value);
    };

    const onSubmit = e => {
        if(userId.length === 0){
            alert('userId.length == 0');
            return;
        }
        if(userPw.length === 0){
            alert('userPw.length == 0');
            return;
        }

        setLoading({type:Constant.LOADING_ACTION.PLUS});
        LoginUtil.login(userId, userPw)
        .then((res) => {
            if (res.resultcode === 1) {
                const d = new Date();
                const authPeriod = d.getTime() + (1000 * 60 * 10);

                const us = {
                    "userId": userId,
                    "authToken": "g5XrsTIytG6M+9juHCuzTj4tnKt1B40vMgJR2vaNiho=",
                    "authPeriod": authPeriod
                };
                LoginUtil.setUserStorage(JSON.stringify(us));

                setLoginUser({
                    userId: userId,
                    userName: userId + 'Name',
                    email: userId + '@test.com'
                });

                navigate("/mypage", {replace:true});
            }
        })
        .finally(() => {
            setLoading({type:Constant.LOADING_ACTION.MINUS});
        });
    };

    return (
        <div>
            <div>로그인 페이지</div>
            <div>
                <input value={userId} onChange={onChangeUserId} />
                <input value={userPw} onChange={onChangeUserPw} />
                <button onClick={() => onSubmit()}>login</button>
            </div>
        </div>
    );
};

export default Login;