import React, {useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { GlobalContext } from "../context/index";


const MyPage = () => {
    console.log('01----MyPage');
    const { loginUser } = useContext(GlobalContext);
    
    if (!loginUser) {
        return <Navigate to="/login" replace={true} />;
    }

    console.log(loginUser);

    return (
        <div>
            <div>마이 페이지</div>
            <div>{loginUser.userId}</div>
            <div>{loginUser.userName}</div>
            <div>{loginUser.email}</div>
        </div>
    );
};

export default MyPage;