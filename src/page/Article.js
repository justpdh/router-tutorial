import { useParams } from 'react-router-dom';

const Article = () => {
    const { id } = useParams();
    return (
        <div>
            <h2>게시글 {id}</h2>
            <div>게시글{id}의 내용</div>
        </div>
    );
};

export default Article;