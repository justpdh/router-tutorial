import React, { useContext } from 'react';
import { Outlet, useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next'

import * as Constant from "../context/Constant";
import { GlobalContext } from "../context/index";
import * as LoginUtil from "../util/LoginUtil";

const Layout = () => {
    console.log('01----Layout');
    const { loginUser, setLoginUser, setLoading, language, setLanguage } = useContext(GlobalContext);

    const navigate = useNavigate();
    const { t } = useTranslation();

    const goLogout = () => {
        setLoading({type:Constant.LOADING_ACTION.PLUS});
        LoginUtil.logout()
        .then((res) => {
            console.log(res);
            if (res.resultcode === 1) {
                LoginUtil.removeUserStorage();
                setLoginUser(null);
                navigate("/");
            }
        })
        .finally(() => {
            setLoading({type:Constant.LOADING_ACTION.MINUS});
        });
    };

    const userLanguageList = [
        { 'lang': 'en', 'langDesc': 'English' },
        { 'lang': 'ko', 'langDesc': 'Korean' },
        { 'lang': 'de', 'langDesc': 'Germann' },
        { 'lang': 'it', 'langDesc': 'Italian' }
    ];

    return (
        <div>
            <header style={{ background: 'lightgray', padding: 16, fontSize: 24 }}>
                <button onClick={() => navigate('/')}>{t('HOME')}</button>
                <button onClick={() => navigate(-1)}>{t('BACK')}</button>
                <button onClick={() => navigate('/articles')}>{t('ARTICLES')}</button>
                <button onClick={() => navigate('/login')} style={{ display: loginUser ? 'none' : '' }}>{t('LOGIN')}</button>
                <button onClick={goLogout} style={{ display: loginUser ? '' : 'none' }}>{t('LOGOUT')}</button>
                <button onClick={() => navigate('/mypage')} style={{ display: loginUser ? '' : 'none' }}>{t('MY_PAGE')}</button>
                <select key={language} defaultValue={language}
                    onChange={(e) => {
                        setLanguage({
                            type: Constant.LANGUAGE_ACTION.SET,
                            value: { lang: e.target.value},
                        });
                    }}>
                    {userLanguageList.map((item, idx) => <option key={idx} value={item.lang}>{item.langDesc}</option>)}
                </select>
            </header>
            <main>
                <Outlet />
            </main>
            <footer style={{ background: 'lightgray', padding: 16, fontSize: 24 }}>
                <div>Footer</div>
            </footer>
        </div>
    );
};

export default Layout;


